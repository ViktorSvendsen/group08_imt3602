//sending http request
        @Override
        protected String doInBackground(String...apiPath) {

            String responseString = "";

            //making request in proper format
            HttpClient client = new DefaultHttpClient();
            HttpPost post = new HttpPost(apiPath[0]);


            //API key for authorization
            post.addHeader("Ocp-Apim-Subscription-Key", "3d2a3274779f40198cf33a940cc580bf");

            //Will send JSON in request body
            post.addHeader("Content-Type","application/json");

            //adding pictures to request body
            String userImagePath =  getIntent().getExtras().get(MainActivity.EXTRA_USER_PHOTO).toString();
            String passportImagePath =  getIntent().getExtras().get(MainActivity.EXTRA_PASSPORT_PHOTO).toString();

            FileBody userImageBin = new FileBody(new File(userImagePath));
            FileBody passportImageBin = new FileBody(new File(passportImagePath));

            try {
                //Read file contents to byte arrays
                byte[] userImageBytes = new byte[userImageBin.getInputStream().available()];
                userImageBin.getInputStream().read(userImageBytes);

                byte[] passportImageBytes = new byte[passportImageBin.getInputStream().available()];
                passportImageBin.getInputStream().read(passportImageBytes);

                //Encode the byte arrays as Base64 strings
                String b64EncodedUserImageBytes = Base64.encodeToString(userImageBytes, Base64.NO_WRAP);
                String b64EncodedPassportImageBytes = Base64.encodeToString(passportImageBytes, Base64.NO_WRAP);

                //Create request body
                /*
                 *{
                 *  referenceImage: "base64 encoded data ...",
                 *  probeImage: "base64 encoded data ..."
                 *}
                 */
                JSONObject requestBody = new JSONObject();
                requestBody.put("referenceImage", b64EncodedPassportImageBytes);
                requestBody.put("probeImage", b64EncodedUserImageBytes);

                //Add body to post request
                post.setEntity(new StringEntity(requestBody.toString()));

            } catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }

            try {
                //registering socket
                client.getConnectionManager().getSchemeRegistry().register( new Scheme("https", SSLSocketFactory.getSocketFactory(), 443) );

                //getting response from Mobai's API
                HttpResponse response = client.execute(post);

                int code = response.getStatusLine().getStatusCode();

                if (code == HttpURLConnection.HTTP_OK) {
                    //Request was successful

                    //Get result from response
                    HttpEntity entity = response.getEntity();
                    String tempEntityString = EntityUtils.toString(entity);

                    //Create JSON object
                    /*
                    {
                        score: "0.0"
                    }
                     */
                    JSONObject result = new JSONObject(tempEntityString);
                    String score = result.getString("score");

                    responseString = score;
                    entity.consumeContent();
                }

                client.getConnectionManager().shutdown();
            }
            catch (IOException e){
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }

            return responseString;
        }